import React from 'react'
import useForm from './useForm';
import validate from './validateInfo';
import './Login.css';



const Login = () => {
    const{handleChange , values, handleSubmit,errors} = useForm(validate);
    return (
      

        <div className= "form-container">


<div className="form-content-left">
    <img src="img/bluebg.jpg" alt="spaceship"
    className="form-img" />

<h1 className='form label'>LOGIN </h1>
        <form className='form' onSubmit={handleSubmit}>
      
       

                            <div className='form-inputs'>

                            <label htmlFor='email'
                            className='form-label'>
                            Email :
                            </label>

                            <input id='email'
                            type='email'
                            name='email'   
                            className='form-input'
                            placeholder="Enter your email"
                            value = {values.email}
                            onChange = {handleChange} 
                            />
                             {errors.email && <p>{errors.email}</p>}  

                            </div>

                            <div className='form-inputs'>

                            <label htmlFor='password'
                            className='form-label'>
                            Password :
                            </label>

                            <input  id='password'
                            type='password'
                            name='password'
                            className='form-input'
                            placeholder="Enter your password"
                            value={values.password}
                            onChange={handleChange}
                            />
                            {errors.password && <p>{errors.password}</p>}    

                            </div>
                           <button className='form-input-btn' 
                           type='submit'>
                                Login
                           </button>


        </form>









</div>


        </div>
      

       
    )

}

export default Login
